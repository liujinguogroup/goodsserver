package com.ljg.goodsserver.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljg.goodsserver.pojo.SwiperTblDTO;
/**
 * <p>
 * 轮播图 Mapper 接口
 * </p>
 *
 * @author LiuJG 344009799@qq.com
 * @since 2022-12-04
 */
public interface SwiperTblMapper extends BaseMapper<SwiperTblDTO> {

}
