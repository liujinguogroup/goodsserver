package com.ljg.goodsserver.pojo;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.experimental.Tolerate;
/**
 * <p>
 * 
 * </p>
 *
 * @author LiuJG 344009799@qq.com
 * @since 2022-12-04
 */
@Data
@Builder
@ApiModel(value = "GoodsTblDTO",description = "")
@TableName(value = "goods_tbl", autoResultMap = true)
public class GoodsTblDTO{

	@Tolerate
	public GoodsTblDTO(){
	}
	@ApiModelProperty("主键")
	@TableId(value="id", type= IdType.AUTO)
	private Long id;

	@ApiModelProperty("商品名称")
	@TableField("goods_name")
	private String goodsName;

	@ApiModelProperty("价格")
	@TableField("price")
	private BigDecimal price;

	@ApiModelProperty("商品主图")
	@TableField("goods_image")
	private String goodsImage;

	@ApiModelProperty("商品详情")
	@TableField("goods_detail")
	private String goodsDetail;

}
