package com.ljg.goodsserver.pojo;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.experimental.Tolerate;
/**
 * <p>
 * 
 * </p>
 *
 * @author LiuJG 344009799@qq.com
 * @since 2022-12-04
 */
@Data
@Builder
@ApiModel(value = "GoodsActivityTblDTO",description = "")
@TableName(value = "goods_activity_tbl", autoResultMap = true)
public class GoodsActivityTblDTO{

	@Tolerate
	public GoodsActivityTblDTO(){
	}
	@ApiModelProperty("主键")
	@TableId(value="id", type= IdType.AUTO)
	private Long id;

	@ApiModelProperty("商品Id ")
	@TableField("goods_id")
	private Long goodsId;

	@ApiModelProperty("活动ID")
	@TableField("activity_id")
	private Integer activityId;

	@ApiModelProperty("活动价格")
	@TableField("activity_price")
	private BigDecimal activityPrice;

}
