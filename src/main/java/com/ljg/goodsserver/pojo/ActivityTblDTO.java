package com.ljg.goodsserver.pojo;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.experimental.Tolerate;
/**
 * <p>
 * 
 * </p>
 *
 * @author LiuJG 344009799@qq.com
 * @since 2022-12-04
 */
@Data
@Builder
@ApiModel(value = "ActivityTblDTO",description = "")
@TableName(value = "activity_tbl", autoResultMap = true)
public class ActivityTblDTO{

	@Tolerate
	public ActivityTblDTO(){
	}
	@TableId(value="id", type= IdType.AUTO)
	private Long id;

	@ApiModelProperty("活动名称")
	@TableField("activity_name")
	private String activityName;

	@ApiModelProperty("活动类型")
	@TableField("activity_type")
	private Integer activityType;

	@ApiModelProperty("活动开始时间")
	@TableField("start_time")
	private Date startTime;

	@ApiModelProperty("活动开始时间")
	@TableField("end_time")
	private Date endTime;

	@ApiModelProperty("创建时间")
	@TableField(value = "create_time", fill = FieldFill.INSERT)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date createTime;

	@ApiModelProperty("修改时间")
	@TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date updateTime;

	@ApiModelProperty("是否删除")
	@TableField("is_del")
	@TableLogic(value="0",delval="1")
	private Integer isDel;

	@ApiModelProperty("描述")
	@TableField("discribe")
	private String discribe;

}
