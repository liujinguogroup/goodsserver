package com.ljg.goodsserver.pojo;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.experimental.Tolerate;
/**
 * <p>
 * 轮播图
 * </p>
 *
 * @author LiuJG 344009799@qq.com
 * @since 2022-12-04
 */
@Data
@Builder
@ApiModel(value = "SwiperTblDTO",description = "轮播图")
@TableName(value = "swiper_tbl", autoResultMap = true)
public class SwiperTblDTO{

	@Tolerate
	public SwiperTblDTO(){
	}
		private Long id;

	@ApiModelProperty("轮播图片")
	@TableField("swiper_pic")
	private String swiperPic;

	@ApiModelProperty("轮播图类型")
	@TableField("swiper_type")
	private Integer swiperType;

	@ApiModelProperty("业务关联内容")
	@TableField("context")
	private String context;

	@TableField(value = "create_time", fill = FieldFill.INSERT)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date createTime;

	@TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date updateTime;

	@ApiModelProperty("是否显示")
	@TableField("is_show")
	private Integer isShow;

}
