package com.ljg.goodsserver.pojo;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.experimental.Tolerate;
/**
 * <p>
 * 分类导航
 * </p>
 *
 * @author LiuJG 344009799@qq.com
 * @since 2022-12-04
 */
@Data
@Builder
@ApiModel(value = "CategoryNavigationTblDTO",description = "分类导航")
@TableName(value = "category_navigation_tbl", autoResultMap = true)
public class CategoryNavigationTblDTO{

	@Tolerate
	public CategoryNavigationTblDTO(){
	}
	@ApiModelProperty("主键")
			@TableId("nav_id")
	private Long navId;

	@ApiModelProperty("分类导航名称")
	@TableField("name")
	private String name;

	@ApiModelProperty("分类导航图标")
	@TableField("image")
	private String image;

	@ApiModelProperty("类型（扩展）")
	@TableField("type")
	private Integer type;

}
