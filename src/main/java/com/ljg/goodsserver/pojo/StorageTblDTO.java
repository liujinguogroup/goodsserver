package com.ljg.goodsserver.pojo;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.experimental.Tolerate;
/**
 * <p>
 * 
 * </p>
 *
 * @author LiuJG 344009799@qq.com
 * @since 2022-12-04
 */
@Data
@Builder
@ApiModel(value = "StorageTblDTO",description = "")
@TableName(value = "storage_tbl", autoResultMap = true)
public class StorageTblDTO{

	@Tolerate
	public StorageTblDTO(){
	}
	@TableId(value="id", type= IdType.AUTO)
	private Long id;

	@ApiModelProperty("商品编号")
	@TableField("goods_id")
	private Long goodsId;

	@ApiModelProperty("商品库存")
	@TableField("count")
	private Integer count;

}
