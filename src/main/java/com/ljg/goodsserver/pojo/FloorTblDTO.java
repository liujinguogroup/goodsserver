package com.ljg.goodsserver.pojo;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.experimental.Tolerate;
/**
 * <p>
 * 
 * </p>
 *
 * @author LiuJG 344009799@qq.com
 * @since 2022-12-04
 */
@Data
@Builder
@ApiModel(value = "FloorTblDTO",description = "")
@TableName(value = "floor_tbl", autoResultMap = true)
public class FloorTblDTO{

	@Tolerate
	public FloorTblDTO(){
	}
	@TableId("floor_id")
	private Long floorId;

	@ApiModelProperty("图片")
	@TableField("image")
	private String image;

	@ApiModelProperty("页面路径")
	@TableField("page_path")
	private String pagePath;

	@ApiModelProperty("图片自定义宽度")
	@TableField("image_wight")
	private Integer imageWight;

	@ApiModelProperty("父楼层ID")
	@TableField("parent_floor_id")
	private Integer parentFloorId;

	@ApiModelProperty("楼层名称")
	@TableField("floor_name")
	private String floorName;

	@ApiModelProperty("楼层数 1表示一级楼层")
	@TableField("floor_num")
	private Integer floorNum;

}
