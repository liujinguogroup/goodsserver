package com.ljg.goodsserver.services.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ljg.goodsserver.mappers.CategoryNavigationTblMapper;
import com.ljg.goodsserver.pojo.SwiperTblDTO;
import com.ljg.goodsserver.services.ISwiperTblService;
import com.ljg.goodsserver.mappers.SwiperTblMapper;
/**
 * <p>
 * 轮播图 服务实现类
 * </p>
 *
 * @author LiuJG 344009799@qq.com
 * @since 2022-12-04
 */
@Service
public class SwiperTblServiceImpl extends ServiceImpl<SwiperTblMapper, SwiperTblDTO> implements ISwiperTblService  {

}
