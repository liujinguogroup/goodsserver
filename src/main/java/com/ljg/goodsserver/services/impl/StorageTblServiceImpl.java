package com.ljg.goodsserver.services.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ljg.common.utils.exception.CustomException;
import com.ljg.goodsserver.mappers.StorageTblMapper;
import com.ljg.goodsserver.pojo.StorageTblDTO;
import com.ljg.goodsserver.services.StorageTblService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
public class StorageTblServiceImpl extends ServiceImpl<StorageTblMapper, StorageTblDTO> implements StorageTblService {

    @Autowired
    private StorageTblMapper storageTblMapper;

    @Override
    @Transactional
    public void deduct(Long goodsId, int count) throws CustomException {
        log.info("开始扣减库存");
        try {
//            storageTblMapper.reduce(goodsId, count);
        } catch (Exception e) {
            throw new CustomException("扣减库存失败，可能是库存不足！");
        }
        log.info("扣减库存成功");
    }

    @Override
    public void editStorage(Long goodsId, int count) {
        QueryWrapper<StorageTblDTO> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(StorageTblDTO::getGoodsId,goodsId);
        StorageTblDTO dto = storageTblMapper.selectOne(wrapper);
        if (dto == null){
            StorageTblDTO storageTblDTO = new StorageTblDTO();
            storageTblDTO.setCount(count);
            storageTblMapper.insert(storageTblDTO);
        }else {
            dto.setCount(count);
            storageTblMapper.updateById(dto);
        }
    }
}