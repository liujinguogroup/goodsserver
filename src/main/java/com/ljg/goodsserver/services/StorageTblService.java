package com.ljg.goodsserver.services;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ljg.common.utils.exception.CustomException;
import com.ljg.goodsserver.pojo.StorageTblDTO;

public interface StorageTblService extends IService<StorageTblDTO>{

    void deduct(Long goodsId, int count) throws CustomException;

    void editStorage(Long goodsId, int count);

}