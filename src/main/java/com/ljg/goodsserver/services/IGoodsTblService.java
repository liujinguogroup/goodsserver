package com.ljg.goodsserver.services;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ljg.goodsserver.pojo.GoodsTblDTO;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LiuJG 344009799@qq.com
 * @since 2022-12-04
 */
public interface IGoodsTblService extends IService<GoodsTblDTO>{
}
