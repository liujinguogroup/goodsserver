package com.ljg.goodsserver.api;

import com.ljg.common.utils.response.ResponseResult;

import com.ljg.goodsserver.pojo.CategoryNavigationTblDTO;
import com.ljg.goodsserver.pojo.FloorTblDTO;
import com.ljg.goodsserver.pojo.SwiperTblDTO;
import com.ljg.goodsserver.services.ICategoryNavigationTblService;
import com.ljg.goodsserver.services.IFloorTblService;
import com.ljg.goodsserver.services.ISwiperTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "小程序接口")
@RestController
public class AppletsIndexController {

    @Autowired
    private ICategoryNavigationTblService categoryNavigationTblService;

    @Autowired
    private IFloorTblService floorTblService;

    @Autowired
    private ISwiperTblService swiperTblService;

    @ApiOperation(value = "首页 - 分类导航")
    @PostMapping("/appletsIndex/cateGoryNavlist")
    public ResponseResult<List<CategoryNavigationTblDTO>> cateGoryNavlist() {
        return ResponseResult.success(categoryNavigationTblService.list());
    }

    @ApiOperation(value = "首页 - 楼层导航")
    @PostMapping("/appletsIndex/floorList")
    public ResponseResult<List<FloorTblDTO>> floorList() {
        return ResponseResult.success(floorTblService.list());
    }

    @ApiOperation(value = "首页 - 轮播图")
    @PostMapping("/appletsIndex/swiperList")
    public ResponseResult<List<SwiperTblDTO>> swiperList() {
        return ResponseResult.success(swiperTblService.list());
    }



}
