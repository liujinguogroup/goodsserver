package com.ljg.goodsserver.api;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ljg.common.utils.exception.CustomException;
import com.ljg.common.utils.response.ResponseResult;
import com.ljg.esserver.entities.GoodsTblES;
import com.ljg.esserver.wrappers.GoodsDocWrapper;
import com.ljg.goodsserver.services.StorageTblService;
import com.ljg.goodsserver.models.StorageReduceModel;
import com.ljg.goodsserver.pojo.GoodsActivityTblDTO;
import com.ljg.goodsserver.pojo.GoodsTblDTO;
import com.ljg.goodsserver.pojo.StorageTblDTO;
import com.ljg.goodsserver.services.IGoodsActivityTblService;
import com.ljg.goodsserver.services.IGoodsTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@Api(tags = "商品接口")
@RestController
public class GoodsController {

    @Autowired
    private StorageTblService storageService;

    @Autowired
    private IGoodsTblService goodsTblService;

    @Autowired
    private IGoodsActivityTblService goodsActivityTblService;

    @Autowired
    private GoodsDocWrapper goodsDocWrapper;

    @ApiOperation(value = "扣减库存")
    @ApiParam(value = "参数实体", name = "model", type = "com.ljg.goodsserver.models.StorageReduceModel")
    @PostMapping("/goods/deduct")
    public ResponseResult<Boolean> deduct(@RequestBody StorageReduceModel model) throws CustomException {
        storageService.deduct(model.getGoodsId(), model.getCount());
        return ResponseResult.success(true);
    }


    @ApiOperation(value = "通过商品号查查商品")
    @PostMapping("/goods/getGoodsById")
    public ResponseResult<GoodsTblDTO> getGoodsById(Long goodsId) {
        return ResponseResult.success(goodsTblService.getById(goodsId));
    }

    @ApiOperation(value = "商品分页")
    @PostMapping("/goods/pageList")
    public ResponseResult<GoodsTblDTO> pageList(Long goodsId) {
        return ResponseResult.success(goodsTblService.getById(goodsId));
    }

    @ApiOperation(value = "通过商品号查查商品库存信息")
    @PostMapping("/goods/getGoodsStorageById")
    public ResponseResult<StorageTblDTO> getGoodsStorageById(@RequestBody Long goodsId) {
        QueryWrapper<StorageTblDTO> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(StorageTblDTO::getGoodsId,goodsId);
        return ResponseResult.success(storageService.getOne(queryWrapper));
    }

    @ApiOperation(value = "通过商品ID查询商品参加的活动列表")
    @PostMapping("/goods/getActivityByGoodsId")
    public ResponseResult<List<GoodsActivityTblDTO>> getActivityByGoodsId(Long goodsId) {
        QueryWrapper<GoodsActivityTblDTO> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(GoodsActivityTblDTO::getGoodsId,goodsId);
        return ResponseResult.success(goodsActivityTblService.list(queryWrapper));
    }

    @ApiOperation(value = "保存商品")
    @ApiParam(value = "参数实体", name = "model", type = "com.example.ljg.models.StorageReduceModel")
    @PostMapping("/goods/savegoods")
    public ResponseResult<Boolean> savegoods(@RequestBody GoodsTblES dto1) throws CustomException {
        goodsDocWrapper.save(dto1);
        return ResponseResult.success(true);
    }




}
