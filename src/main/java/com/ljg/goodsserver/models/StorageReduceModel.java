package com.ljg.goodsserver.models;

import lombok.Data;

import java.io.Serializable;

@Data
public class StorageReduceModel implements Serializable {

    private Long goodsId;

    private Integer count;

}
