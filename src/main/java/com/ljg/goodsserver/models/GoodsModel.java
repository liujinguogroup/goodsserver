package com.ljg.goodsserver.models;

import lombok.Builder;
import lombok.Data;


import java.io.Serializable;

@Data
@Builder
public class GoodsModel implements Serializable {

    private Long id;
    private String commodityCode;
    private String goodsName;
    private Integer goodsType;
    private Integer count;
}
