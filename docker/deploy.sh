harbor_addr=$1
harbor_repo=$2
project=$3
version=$4
server_port=$5
container_port=$6

imageName=$harbor_addr/$harbor_repo/$project:$version
echo "当前镜像" $imageName

containerId=`docker ps -a | grep $project | awk '{print $1}'`
echo "当前容器ID:" $containerId

if [ "$containerId" != "" ] ; then
   docker stop $containerId
   docker rm $containerId
   echo "删除容器成功"
fi
tag=`docker images | grep ${project} | awk '{print $2}'`
echo $tag
if [[ "$tag" =~ "$version" ]] ; then
  docker rmi $imageName
  echo "删除镜像成功"
fi
docker login -u admin -p Harbor12345 $harbor_addr
echo "harbor 登录成功"
docker pull $imageName
echo "镜像重新拉取成功"
docker run -d -p $container_port:$server_port --name $project $imageName
echo "success"


